#!/usr/bin/env python3.4 -B

import numbers
from pyrser import meta
from pyrser.parsing.node import Node

def dump_set(toSort, deep):
	
	result = ""

	for item in sorted(toSort):
		if isinstance(item, str):
			result += "%s<'%s'/>\n" % ("\t" * deep, item)
		else:
			result += "%s<%s/>\n" % ("\t" * deep, item)

	return (result)

def dump_dict(toSort, deep):

	result = ""

	for key, value in sorted(toSort.items()):


		if isinstance(value, object) and hasattr(value, '__dict__'):
			result += "%s<.idx __key = '%s' type = %s>\n" % ("\t" * deep, key, "object")
			result += to_dxml(value, deep + 1)
			result += "%s</.idx>\n" % ("\t" * deep)


		elif isinstance(value, (list, dict, set)):
			result += "%s<.idx __key = '%s' type = %s>\n" % ("\t" * deep, key, value.__class__.__name__)
			result += eval("dump_" + value.__class__.__name__)(value, deep + 1)
			result += "%s</.idx>\n" % ("\t" * deep)

		# Other
		else:
			if value.__class__.__name__ == "NoneType":
				result += "%s<.idx __key = '%s'/>\n" % ("\t" * deep, key)
			elif isinstance(value, str):
				result += "%s<.idx __key = '%s' %s = '%s'/>\n" % ("\t" * deep, key, value.__class__.__name__, value)
			elif isinstance(value, bytes):
				result += "%s<.idx __key = '%s' type = blob>\n%s" % ("\t" * deep, key, "\t" * (deep + 1))
				for b in value:
					result += "%02X " % (b)
				result += "\n%s</.idx>\n" % ("\t" * deep)
			else:
				result += "%s<.idx __key = '%s' %s = %s/>\n" % ("\t" * deep, key, value.__class__.__name__, value)

	return (result)

def dump_list(toSort, deep):

	result = ""

	for i, item in enumerate(toSort):

		if isinstance(item, object) and hasattr(item, '__dict__'):
			result += "%s<.idx __value = %s type = %s>\n" % ("\t" * deep, i, "object")
			result += to_dxml(item, deep + 1)
			result += "%s</.idx>\n" % ("\t" * deep)

		elif isinstance(item, (list, dict, set)):
			result += "%s<.idx __value = %s type = %s>\n" % ("\t" * deep, i, item.__class__.__name__)
			result += eval("dump_" + item.__class__.__name__)(item, deep + 1)
			result += "%s</.idx>\n" % ("\t" * deep)

		# Other
		else:
			if item.__class__.__name__ == "NoneType":
				result += "%s<.idx __value = %s/>\n" % ("\t" * deep, i)
			elif isinstance(item, str):
				result += "%s<.idx __value = %s %s = '%s'/>\n" % ("\t" * deep, i, item.__class__.__name__, item)
			elif isinstance(item, bytes):
				result += "%s<.idx __value = %s type = blob>\n%s" % ("\t" * deep, i, "\t" * (deep + 1))
				for b in item:
					result += "%02X " % (b)
				result += "\n%s</.idx>\n" % ("\t" * deep)
			else:
				result += "%s<.idx __value = %s %s = %s/>\n" % ("\t" * deep, i, item.__class__.__name__, item)
		i += 1

	return (result)

@meta.add_method(Node)
def to_dxml(self, deep = 1):

	result = ""

	if deep == 1:
		result += "<.root type = object>\n"

	for var, value in sorted(self.__dict__.items()):
		instance = getattr(self, var)

		# Object
		if isinstance(instance, object) and hasattr(instance, '__dict__'):
			result += "%s<%s type = %s>\n" % ("\t" * deep, var, "object")
			result += to_dxml(instance, deep + 1)
			result += "%s</%s>\n" % ("\t" * deep, var)

		elif isinstance(instance, (list, dict, set)):
			result += "%s<%s type = %s>\n" % ("\t" * deep, var, instance.__class__.__name__)
			result += eval("dump_" + instance.__class__.__name__)(instance, deep + 1)
			result += "%s</%s>\n" % ("\t" * deep, var)

		elif isinstance(instance, bytes):
			result += "%s<%s type = blob>\n%s" % ("\t" * deep, var, "\t" * (deep + 1))
			for b in value:
				result += "%02X " % (b)
			result += "\n%s</%s>\n" % ("\t" * deep, var)

		else:
			if instance.__class__.__name__ == "NoneType":
				result += "%s<%s/>\n" % ("\t" * deep, var)
			else:
				if isinstance(value, str):
					result += "%s<%s %s = '%s'/>\n" % ("\t" * deep, var, instance.__class__.__name__, value)
				else:
					result += "%s<%s %s = %s/>\n" % ("\t" * deep, var, instance.__class__.__name__, value)

	if deep == 1:
		result += "</.root>"

	return (result)